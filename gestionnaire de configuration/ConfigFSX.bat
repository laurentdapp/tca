@echo off
if exist Config.ini (goto begin)

::premier lancer
echo Execution du script pour la premiere fois. 
echo Attendez que les fichiers soit generes.
echo L installation que vous aviez sera deplaces vers Standard.XMLBAK
echo Vous pouvez modifier le fichier Config.ini pour specifier vos propres nommage.
timeout 15 >nul

::Config.ini
echo ;;Settings > Config.ini
echo. >> Config.ini
echo ;;Vous pouvez changer les noms des profils de mod ici. >> Config.ini
echo ;;Standard.XML est pour la configuration actuelle, Standard.XML1 est pour le A320 etc. >> Config.ini
echo ;;Veuillez ne pas utiliser de caracteres speciaux dans les noms. >> Config.ini
echo ;;L utilisation de ces caracteres peut casser le code. >> Config.ini
echo. >> Config.ini
echo Standard.XML1=A320 >> Config.ini
echo Standard.XML2=B737 >> Config.ini
echo Standard.XML3=Bombardier >> Config.ini
echo Standard.XML4=Cesna >> Config.ini
echo Standard.XML5=Piper >> Config.ini
echo Standard.XML6=Bravo >> Config.ini
echo Standard.XML7=Canadair >> Config.ini
echo Standard.XML8=Helicoptere 1 >> Config.ini
echo Standard.XML9=Helicoptere 2 >> Config.ini
echo. >> Config.ini
echo. >> Config.ini
echo ;;Si vous definissez ceci sur true, apres avoir selectionne un profil >> Config.ini
echo ;;il lancera directement le jeu. "C,Q,L" le menu est saute. >> Config.ini
echo DirectLaunch=true >> Config.ini
echo. >> Config.ini
echo. >> Config.ini
echo ;;NE CHANGEZ PAS CELA >> Config.ini
echo Current=0 >> Config.ini
echo ;;NE CHANGEZ PAS CELA >> Config.ini


::Creation de nouveaux fichier
ren Standard.XML Standard.XMLBAK
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML1"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML2"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML3"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML4"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML5"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML6"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML7"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML8"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML9"
echo F|xcopy /F "Standard.XMLBAK" "Standard.XML"
cls
goto begin


:begin
cls
:: Charger le fichier de configuration
FOR /F "usebackq eol=; tokens=* delims=" %%# in (
    "Config.ini"
) do (
    call set "%%#"
)  >nul

:: Changer le profil actuellement selectionne
ren Standard.XML Standard.XML%Current%
ren Standard.XML0 Standard.XML
powershell -Command "(Get-Content Config.ini) -replace 'Current=%Current%', 'Current=0' | Set-Content Config.ini"
powershell -Command "(Get-Content Config.ini) -replace 'Current=00', 'Current=0' | Set-Content Config.ini"
echo Bascule vers le profil par defaut.
:: Liste des noms de profil
echo Quel configuration d avion voulez-vous activer?
echo.
echo 0- [Defaut]  (Default)
echo 1- [%Standard.XML1%]
echo 2- [%Standard.XML2%]
echo 3- [%Standard.XML3%]
echo 4- [%Standard.XML4%]
echo 5- [%Standard.XML5%]
echo 6- [%Standard.XML6%]
echo 7- [%Standard.XML7%]
echo 8- [%Standard.XML8%]
echo 9- [%Standard.XML9%]
echo.

:: Choix menu 1
setlocal
CHOICE /N /C:1234567890 /M "Appuyez sur l'un des boutons (0, 1, 2, 3, 4, 5, 6, 7, 8 or 9)"

IF ERRORLEVEL ==10 GOTO 0
IF ERRORLEVEL ==9 GOTO 9
IF ERRORLEVEL ==8 GOTO 8
IF ERRORLEVEL ==7 GOTO 7
IF ERRORLEVEL ==6 GOTO 6
IF ERRORLEVEL ==5 GOTO 5
IF ERRORLEVEL ==4 GOTO 4
IF ERRORLEVEL ==3 GOTO 3
IF ERRORLEVEL ==2 GOTO 2
IF ERRORLEVEL ==1 GOTO 1
endlocal

:: Renommeurs de fichiers
:0
echo Appuye sur 0
echo Actuellement defaut est selectionnee.
goto end
:1
echo Appuye sur 1
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=1' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML1 Standard.XML
echo Currently %Standard.XML1% is selected.
goto end
:2
echo Appuye sur 2
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=2' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML2 Standard.XML
echo Currently %Standard.XML2% is selected.
goto end
:3
echo Appuye sur 3
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=3' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML3 Standard.XML
echo Currently %Standard.XML3% is selected.
goto end
:4
echo Appuye sur 4
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=4' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML4 Standard.XML
echo Currently %Standard.XML4% is selected.
goto end
:5
echo Appuye sur 5
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=5' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML5 Standard.XML
echo Currently %Standard.XML5% is selected.
goto end
:6
echo Appuye sur 6
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=6' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML6 Standard.XML
echo Currently %Standard.XML6% is selected.
goto end
:7
echo Appuye sur 7
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=7' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML7 Standard.XML
echo Currently %Standard.XML7% is selected.
goto end
:8
echo Appuye sur 8
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=8' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML8 Standard.XML
echo Currently %Standard.XML8% is selected.
goto end
:9
echo Appuye sur 9
powershell -Command "(Get-Content Config.ini) -replace 'Current=0', 'Current=9' | Set-Content Config.ini"
ren Standard.XML Standard.XML0
ren Standard.XML9 Standard.XML
echo Currently %Standard.XML9% is selected.
goto end

:end
::Skipper
if %DirectLaunch%==true ( goto l )

:List2
echo.
echo c- [Changer]
echo l- [Lancer ]
echo q- [Quitter]
echo.
:: choix menu 2
setlocal
CHOICE /N /C:clq /M "Appuyez sur l un des boutons (C, L ou Q)"

IF ERRORLEVEL ==3 GOTO q
IF ERRORLEVEL ==2 GOTO l
IF ERRORLEVEL ==1 GOTO begin

if %fin%==c goto begin
if %fin%==q goto q
if %fin%==l goto l
endlocal

:l
start /D "C:\Program Files (x86)\Microsoft Games\Microsoft Flight Simulator X" fsx.exe
:q
exit
