# TCA

Partage et sauvegarde de fichiers de configuration des Manettes Thrustmaster TCA.

Sharing and saving Thrustmaster TCA Controller configuration files.

## Manettes actuelles / Current controllers

Pour l'instant je me focalise sur les manettes TCA Captain Pack que je viens d'acquérir.

For the moment I am focusing on the TCA Captain Pack controllers that I have just acquired.

### Manette en stock / Controller in stock

Je possede une manette ''TOP GUN'' que je resortirais pour vous proposer une config.

I have a ''TOP GUN'' controller that I would come out to offer you a config.

#### Future manettes / Future controllers

Si mon patron m'accorde une augmantation un jour (c'est pas gagné) je me procurerais le pédalier (un peu cher pour moi)

If my boss gives me a raise one day (it's not won) I would get the pedals (a bit expensive for me)

##### INFO's

Pour l'instant le fichier "TCA Captain Pack(TARGET).fcf" est a peu pres fonctionnel, il me reste une difficulté avec le spoiler que j'ai du mal a faire fonctionné mais dans l'ensemble c'est jouable.

J'ajouterais au fur et a mesure de nouveaux avions (les plus répendu).

For the moment the file "TCA Captain Pack (TARGET).fcf" is almost functional, I still have a difficulty with the spoiler that I have trouble getting to work but overall it's playable.

I would add as and when new planes (the most widespread).
